<?php
require '../Model/clientemodel.php';
class clienteController{
	public function verificaClave() {
		$this->model = new clienteModel();
		$this->result = $this->model->verificaClave();
		return $this->result;	}
	public function nombreLaboModo($cod) {
		$this->model = new clienteModel();
		$this->result = $this->model->nombreLaboModo($cod);
		return $this->result;
	}
	public function actualizarModo($cod,$modo){
		$this->model = new clienteModel();
		$this->result = $this->model->actualizarModo($cod,$modo);
		return $this->result;
	}
	// Para obtener imagenes de las luminarias
	public function estadosLumi($cod) {
		$this->model = new clienteModel();
		$this->result = $this->model->estadosLumi($cod);
		return $this->result;
	}	
	public function actualizarLumi1($cod,$estado){
		$this->model = new clienteModel();
		$this->result = $this->model->actualizarLumi1($cod,$estado);
		return $this->result;
	}
	public function actualizarLumi2($cod,$estado){
		$this->model = new clienteModel();
		$this->result = $this->model->actualizarLumi2($cod,$estado);
		return $this->result;
	}
	public function actualizarLumi3($cod,$estado){
		$this->model = new clienteModel();
		$this->result = $this->model->actualizarLumi3($cod,$estado);
		return $this->result;
	}
	public function actualizarLumi4($cod,$estado){
		$this->model = new clienteModel();
		$this->result = $this->model->actualizarLumi4($cod,$estado);
		return $this->result;
	}
	public function autoLumi($cod) {
		$this->model = new clienteModel();
		$this->result = $this->model->autoLumi($cod);
		return $this->result;
	}
}
?>